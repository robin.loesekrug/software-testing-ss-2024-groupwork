package com.softwaretesting.testing.customerRegistration.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.exception.BadRequestException;
import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CustomerRegistrationServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerRegistrationService customerRegistrationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    // Test case for successful customer registration
    @Test
    void registerNewCustomer_Success() {
        // Arrange
        Customer customer = new Customer(Long.parseLong("1001"),"test_user","Test User", "+490001698");

        // Mocking behavior of CustomerRepository
        when(customerRepository.selectCustomerByPhoneNumber(any())).thenReturn(Optional.empty());
        when(customerRepository.save(any())).thenReturn(customer);

        // Act
        Customer registeredCustomer = customerRegistrationService.registerNewCustomer(customer);

        // Assertions
        assertNotNull(registeredCustomer);
        assertEquals("Test User", registeredCustomer.getName());
        assertEquals("+490001698", registeredCustomer.getPhoneNumber());

        // Verifying interactions with the mocked repository
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber(any());
        verify(customerRepository, times(1)).save(any());
    }

    // Test case for registering a customer with an existing phone number
    @Test
    void registerNewCustomer_WithExistingPhoneNumber() {
        // Arrange
        Customer existingCustomer = new Customer(Long.parseLong("1001"),"test_user","Test User", "+490001698");

        Customer newCustomer = new Customer(Long.parseLong("1002"),"test_user_2","Test User 2", "+490001698");

        // Mocking behavior of CustomerRepository
        when(customerRepository.selectCustomerByPhoneNumber("+490001698")).thenReturn(Optional.of(existingCustomer));

        // Calling the method under test and expecting an exception
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {
            customerRegistrationService.registerNewCustomer(newCustomer);
        });

        // Verifying that the correct exception is thrown with the correct message
        assertEquals("Phone Number +490001698 taken", exception.getMessage());

        // Verifying interactions with the mocked repository
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber("+490001698");
        verify(customerRepository, never()).save(any());
    }

    // Test case for registering a customer with the same name as an existing customer
    @Test
    void registerNewCustomer_WithSameName() {
        // Arrange
        Customer existingCustomer = new Customer(Long.parseLong("1001"),"test_user","Test User", "+490001698");

        Customer newCustomer = new Customer(Long.parseLong("1002"),"test_user_2","Test User", "+490001699");

        // Mocking behavior of CustomerRepository
        when(customerRepository.selectCustomerByPhoneNumber("+490001699")).thenReturn(Optional.of(existingCustomer));

        // Calling the method under test and expecting an exception
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            customerRegistrationService.registerNewCustomer(newCustomer);
        });

        // Verifying that the correct exception is thrown with the correct message
        assertEquals("You are already registered", exception.getMessage());

        // Verifying interactions with the mocked repository
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber("+490001699");
        verify(customerRepository, never()).save(any());
    }

}
