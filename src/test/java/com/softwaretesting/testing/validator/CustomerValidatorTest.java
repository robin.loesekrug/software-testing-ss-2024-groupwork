package com.softwaretesting.testing.validator;

import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;


class CustomerValidatorTest {
    private Customer customer;

    private final CustomerValidator customerValidator = new CustomerValidator();

    @Test
    void validate404_ThrowsException_WhenOptionalIsEmpty() {
        // Arrange
        Optional<Object> emptyOptional = Optional.empty();
        String label = "username";
        String value = "Jacob";

        // Act
        ResponseStatusException thrown = assertThrows(ResponseStatusException.class, () ->
                customerValidator.validate404(emptyOptional, label, value)
        );

        // Assert the status code and message
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
        assertTrue(thrown.getReason().contains("Optional with username'Jacob' does not exist."));
    }

    @Test
    void validate404_DoesNotThrowException_WhenOptionalIsPresent() {
        // Arrange
        Optional<Object> nonEmptyOptional = Optional.of(new Customer(Long.parseLong("1000"),"test_1","Test Customer 1","+4915112345678"));
        String label = "username";
        String value = "test_1";

        // Act & Assert
        assertDoesNotThrow(() -> customerValidator.validate404(nonEmptyOptional, label, value));
    }

}
